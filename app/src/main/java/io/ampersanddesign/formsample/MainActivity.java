package io.ampersanddesign.formsample;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        populateData();
    }

    private void populateData() {
        List<String> sectionNames = loadFormDetails();
        ViewPager pager = findViewById(R.id.viewPager);
        pager.setAdapter(new FormAdapter(getSupportFragmentManager(), sectionNames));
    }

    private List<String> loadFormDetails() {
        ArrayList<String> sectionNames = new ArrayList<>();
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray sections = obj.getJSONArray("sections");
            for (int i=0; i<sections.length(); i++) {
                JSONObject section = sections.getJSONObject(i);
                sectionNames.add(section.getString("name"));
            }
            Log.d("Form", obj.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sectionNames;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("form_details.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private static class FormAdapter extends FragmentStatePagerAdapter {

        private List<String> sections;

        public FormAdapter(FragmentManager fm, List<String> sections) {
            super(fm);
            this.sections = sections;
        }

        @Override
        public Fragment getItem(int position) {
            return FormFragment.newInstance(position + 1, getCount(), sections.get(position));
        }

        @Override
        public int getCount() {
            return sections.size();
        }
    }

    public static class FormFragment extends Fragment {

        private static final String KEY_INDEX = "index";
        private static final String KEY_TOTAL = "total";
        private static final String KEY_NAME = "name";

        TextView indexView;
        TextView nameView;

        private String name;
        private int index, total;

        public static FormFragment newInstance(int index, int total, String name) {
            FormFragment f = new FormFragment();
            Bundle args = new Bundle();
            args.putInt(KEY_INDEX, index);
            args.putInt(KEY_TOTAL, total);
            args.putString(KEY_NAME, name);
            f.setArguments(args);
            return f;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            index = getArguments().getInt(KEY_INDEX);
            total = getArguments().getInt(KEY_TOTAL);
            name = getArguments().getString(KEY_NAME);
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_form, container, false);
            indexView = rootView.findViewById(R.id.index);
            nameView = rootView.findViewById(R.id.content);
            return rootView;
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            indexView.setText(String.format("%d of %d", index, total));
            nameView.setText(name);
        }
    }
}
